baudrate=115200
board_name=EVK
board_rev=iMX8MM
boot_fit=no
bootcmd=mmc dev ${mmcdev}; if mmc rescan; then if run loadbootscript; then run bootscript; else if run loadimage; then run mmcboot; else run netboot; fi; fi; fi;
bootdelay=2
bootm_size=0x10000000
bootscript=echo Running bootscript from mmc ...; source
console=ttymxc1,115200
ethaddr=00:04:9f:06:ae:09
ethprime=FEC
fdt_addr=0x43000000
fdt_file=imx8mmevk/oftree
fdtcontroladdr=bbf52930
gatewayip=192.168.42.254
hostname=imx8mmevk
image=imx8mmevk/Image
initrd_addr=0x43800000
ipaddr=192.168.42.77
loadaddr=0x40480000
loadbootscript=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${script};
loadfdt=fatload mmc ${mmcdev}:${mmcpart} ${fdt_addr} ${fdt_file}
loadimage=fatload mmc ${mmcdev}:${mmcpart} ${loadaddr} ${image}
mmcargs=setenv bootargs console=${console} root=${mmcroot}
mmcautodetect=yes
mmcboot=echo Booting from mmc ...; run mmcargs; if test ${boot_fit} = yes || test ${boot_fit} = try; then bootm ${loadaddr}; else if run loadfdt; then booti ${loadaddr} - ${fdt_addr}; else echo WARN: Cannot load the DT; fi; fi;
mmcdev=1
mmcpart=1
mmcroot=/dev/mmcblk1p2 rootwait rw
netargs=setenv bootargs console=${console} root=/dev/nfs ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp
netboot=echo Booting from net ...; run netargs;  if test ${ip_dyn} = yes; then setenv get_cmd dhcp; else setenv get_cmd tftp; fi; ${get_cmd} ${loadaddr} ${image}; if test ${boot_fit} = yes || test ${boot_fit} = try; then bootm ${loadaddr}; else if ${get_cmd} ${fdt_addr} ${fdt_file}; then booti ${loadaddr} - ${fdt_addr}; else echo WARN: Cannot load the DT; fi; fi;
nfsroot=/opt/poky/imx8mmevk-rootfs
script=boot.scr
serverip=192.168.42.1
stderr=serial
stdin=serial
stdout=serial

Environment size: 1717/4092 bytes

