#!/bin/bash

pushd /workdir/build/imx8mm-lpddr4-evk-master/tmp/deploy/images/imx8mm-lpddr4-evk

SOURCE_1="core-image-minimal-base-imx8mm-lpddr4-evk*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/imx8mm-lpddr4-evk-master/tmp/deploy/images/imx8mm-lpddr4-evk"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
